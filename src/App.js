import React, { useState, useEffect } from 'react';
import './App.css';



function App() {
  const [data, setdata]= useState({})

  useEffect(()=>{
    fetch ('http://10.1.1.177/api/commande/all')
    .then(response => {
      return response.json()
    })
    .then(rs => {
      return(rs.data)
    })
    .then(result => {
      setdata(result)
    })
    .catch((error) => {
      console.log("Il y a eu un problème");
    });

  }, [])
  return (
    <div className="App">
      <h1>Liste des commandes</h1>
      {console.log('result', data)}
      
      <table aria-label="simple" className="table">
        <tr>
        <td>id</td>
            <td>client</td>
            <td>status</td>
            <td>createdAt</td>
        </tr>
            
        
         {data.map((item) => {
           return(
             <tr>
               <td>{item.id}</td>
              <td>{item.client.codeInsc}</td>
              <td>{item.status}</td>
              <td>{item.createdAt}</td>
             </tr>
           )
           
         })}
        

        
      </table>


    </div>
  );
}

export default App;
